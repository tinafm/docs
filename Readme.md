----------------------------------------------
*** 下载repo
1. download repo
curl https://raw.githubusercontent.com/tinalinux/repo/stable/repo > ~/bin/repo
chmod +x ~/bin/repo
----------------------------------------------
*** sync
1. repo init -u https://git.oschina.net/tinafm/manifest -b r16-v2.1.y -m r16/v2.1.y.xml
2. repo sync
3. repo start r16-v2.1.y --all
----------------------------------------------
*** compile
1. source build/envsetup.sh
2. lunch astar_nbr-tina
3. make -j4
4. pack -d
