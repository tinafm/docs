#!/bin/bash

tina_repos_list=`cat tina_repos_list`
base_path=`pwd`
for repo in ${tina_repos_list[@]}
do
    cd ${repo}
    pwd
    git status
    git pull
    git push origin HEAD:AP1
    cd ${base_path}

done
