﻿#!/usr/bin/env python
import os
import sys
import re
import anydbm
import pickle

def find_commit_No(commit_all):
    for line in commit_all:
        print line
        if line.find('commit') != -1:
            No = line.split(' ')
            return No[1]

def git_tag(tag_version, commit_No):
    cmd = "git tag -a %s -m '%s' %s" % (tag_version,tag_version,commit_No)
    print cmd
    os.popen(cmd)

def git_tag_push(tag_version, branch):
    cmd = "git push origin %s %s" % (tag_version, branch)
    print cmd
    os.popen(cmd)

def find_commit(branch):
    #find the commit_No and get the latest commit information
    commit = os.popen('git log %s -1' % branch)
    commit_all = commit.read()
    fin_single = open('../commit_single.txt', 'w')
    fin_single.write(commit_all)
    fin_single.close()
    #find the latest commit No
    fout_single = open('../commit_single.txt', 'r')
    commit_No = find_commit_No(fout_single)
    fout_single.close()
    return commit_No

def find_last_nth_commit_no(branch, n):
    #find the last nth commit No and get the related commit information
    cmd = 'git log %s -%d' % (branch, n)
    commit_n = os.popen(cmd)
    fin_n = open('../commit_n.txt', 'w')
    fin_n.write(commit_n.read())
    fin_n.close()

def get_full_commit_info(brief_commit, branch):
    get_full_commit = open('../get_full_commit.txt', 'w')
    get_full_commit.write(os.popen('git log %s' % branch).read())
    get_full_commit.close()

    full_commit = open('../get_full_commit.txt', 'r')
    for line in full_commit:
        if line.find(brief_commit) != -1:
            print line
            full_commit.close()
            return line.split(' ')[1]

def get_update_log(repo_name, last_commit_no, branch):
    #get all commit information for this repo
    commitinfo = os.popen('git log %s' % branch)
    commitinfo_all = open('../commitinfo_all.txt', 'w')
    commitinfo_all.write(commitinfo.read())
    commitinfo_all.close()

    #remove the space
    fin = open('../commitinfo_all.txt', 'r')
    fout = open('../commitinfo_all_wo.txt', 'w')
    for line in fin:
        if line.split():
            fout.write(line)
    fin.close()
    fout.close()


    #find the update deta info
    commit_old_no = last_commit_no
    f_log = open('../commitinfo_all_wo.txt', 'r')
    n = 0
    m = 0
    log_sum = []
    for line in f_log:
        if line.find(commit_old_no) != -1:
            break
        else:
            if line.find('Author') != -1:
                print line
                log_line = ''
                m = n+2
                #print m
            if n == m:
                #print n,line
                log_line = line.strip(' \n')
            if n == m+1:
                log_line = log_line+'/'+line.strip(' \n')
                log_sum = log_sum + [log_line]
            n = n+1
    f_log.close
    print log_sum

    #write to file
    update_info = open('../update_log.txt','a')
    for i in log_sum:
        str_i = '%s:%s' % (repo_name,i)
        update_info.write(str_i)
        update_info.write('\n')
    update_info.close()


def create_taginfo_to_db(branch):
    #get the gitlog oneline information
    cmd = 'git log %s --oneline --decorate --graph' % branch
    fin_gitlog_oneline = open('../gitlog_oneline.txt', 'w')
    fin_gitlog_oneline.write(os.popen(cmd).read())
    fin_gitlog_oneline.close()

    #find the last tag information
    gitlog_oneline = open('../gitlog_oneline.txt', 'r')
    log_n = 1
    tag_version_pattern = 'tag: v\d\.\d\d?\.\d{8}v?\d?'
    commit_pattern = '\* [a-z0-9]{7}'
    for line in gitlog_oneline:
        #print re.findall(tag_version_pattern, line)
        if re.findall(tag_version_pattern, line) != []:
            #print 'test'
            last_tag_version = re.findall(tag_version_pattern, line)
            last_commit = re.findall(commit_pattern, line)
            print log_n, last_tag_version[0].strip('tag: '), last_commit[0].strip('* ')
            gitlog_oneline.close()
            return log_n, last_tag_version[0].strip('tag: '), last_commit[0].strip('* ')
        else:
            log_n = log_n+1



def check_commit(repo_name, commit_No, tag_version, branch):
    #check the commit
    db = anydbm.open('../lasttagcommit.db', 'c')
    try:
        #print db[repo_name]
        if pickle.loads(db[repo_name])[0] == commit_No:
            print pickle.loads(db[repo_name])[0]
            print '****************************************'
            print 'No change for this Repo, no need for tag'
            print '****************************************'
        else:
            print '****************************************'
            print 'Tag the repo and update db'
            print '****************************************'
            if pickle.loads(db[repo_name])[1] == tag_version:
                print '****************************************'
                print 'Tag_version is not changed, error'
                print '****************************************'
            else:
                get_update_log(repo_name, pickle.loads(db[repo_name])[0], branch)
                db[repo_name] = pickle.dumps([commit_No, tag_version])
                git_tag(tag_version, commit_No)
                git_tag_push(tag_version, branch)

    except:
        print '****************************************'
        print 'Tag the repo and create to db'
        print '****************************************'
        try:
            log_n, last_tag_version, last_commit = create_taginfo_to_db(branch)
            last_commit_No = get_full_commit_info(last_commit, branch)
            print log_n, last_tag_version, last_commit_No
            if tag_version == last_tag_version:
                print '****************************************'
                print 'Tag version is the same, no need for tag. Create to db using last tag'
                print '****************************************'
                db[repo_name] = pickle.dumps([last_commit_No, last_tag_version])
            else:

                if last_commit_No == commit_No:
                    print '****************************************************************************************'
                    print 'Tag version is new, but no new commit, Create to db using last tag'
                    print '****************************************************************************************'
                    db[repo_name] = pickle.dumps([last_commit_No, last_tag_version])
                else:
                    print '****************************************************************************************'
                    print 'Tag version is new, with new commit, Create to db using new tag and update to repo'
                    print '****************************************************************************************'
                    get_update_log(repo_name, last_commit_No)
                    db[repo_name] = pickle.dumps([commit_No, tag_version])
                    git_tag(tag_version, commit_No)
                    git_tag_push(tag_version, branch)
        except:
            db[repo_name] = pickle.dumps([commit_No, tag_version])
            git_tag(tag_version, commit_No)
            git_tag_push(tag_version, branch)

    db.close()


def main(repo_name, tag_version, branch):
    #get the commit information
    print 'repo_name:', repo_name
    print 'tag_version:',tag_version
    commit_No = find_commit(branch)

    #tag
    #print commit_No
    print os.getcwd()
    check_commit(repo_name, commit_No, tag_version, branch)



if __name__ == "__main__":
    tag_version = 'v0.10.20160721v1'
    print sys.argv
    main(sys.argv[1], sys.argv[2], sys.argv[3])
    