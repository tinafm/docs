#!/bin/bash

if [ -n "$1" ] && [ $1 == tag_enable ]; then
    go_repos_list=(`cat tina_repos_list`)
    echo ${go_repos_list[@]}
    tag_version=`cat ./tinalinux/tagversion.txt`
    echo ${tag_version}
    #main_version=${release_version[0]}
    #minor_version=${release_version[1]}
    #tag_version="${main_version}v${minor_version}"

    DATE="`date +%Y-%m-%d`"
Find

    touch update_log.txt
    echo "---------------------------------------------------------------------------" >> update_log.txt
    echo "## ${tag_version}:${DATE}" >> update_log.txt

    for repo in ${go_repos_list[@]}
    do
        cd ${repo}
        git pull
        python ../getcommitNo.py ${repo} ${tag_version} r16-v2.1.y
        cd ..
    done
    pwd
    mv -f update_log.txt ./rafs_release/${tag_version}/update_log.txt
    if [ -f commitinfo_all.txt ]; then
        mv commitinfo_all.txt ./rafs_release/${tag_version}/commitinfo_all.txt
    fi

    if [ -f commitinfo_all_wo.txt ]; then
        mv -f commitinfo_all_wo.txt ./rafs_release/${tag_version}/commitinfo_all_wo.txt
    fi

    if [ -f commit_single.txt ]; then
        mv -f commit_single.txt ./rafs_release/${tag_version}/commit_single.txt
    fi

    if [ -f gitlog_oneline.txt ]; then
        mv -f gitlog_oneline.txt ./rafs_release/${tag_version}/gitlog_oneline.txt
    fi
else
    echo "Tag all go repos is disable!"
fi

echo "" >> commit_sum.txt
cat ./rafs_release/${tag_version}/update_log.txt >> commit_sum.txt