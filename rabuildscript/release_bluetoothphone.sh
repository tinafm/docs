#!/bin/bash -e

# This script is used to tag all RA releated repos, and build the linux system for RA.
# p0.1.20171018v3: Support chinese wifi ssid in tina system
clear
cnt=1
base_path=`pwd`

# ToDo: Now when build bluetoothphone, need build normal frist
# the RA repo need to update to build bluetoothphone
## ${cnt}. Build RA application
#echo "#############################################################"
#echo "# ${cnt}. Build RA application"
#echo "#############################################################"
#
## Build
#if [ -n "$1" ] && [ $1 == again ]; then
#    echo "Exe build again!"
#    cd ra
#    ./build_again.sh
#else
#    echo "Exe new build!"
#    if [ ! -d ra_base ]; then
#        mkdir ra_base
#        git clone ssh://git.nane.cn:29418/project/ra/firmware/ra ra_base
#    fi
#    cd ra_base
#    git pull
#    cd ${base_path}
#
#    [[ -d ra ]] && rm -rf ra
#    mkdir ra
#    cp -rf ra_base/* ra
#    cd ra
#    ./build.sh
#fi
#cd ${base_path}
#let cnt=cnt+1

# ${cnt}. Clean and change branch tina system before release ra to tina
echo "#############################################################"
echo "# ${cnt}. Clean and change branch tina system before release ra to tina"
echo "#############################################################"
cd tinalinux
./change2bluetooth.sh
cd ${base_path}

# ${cnt}. Release RA  and RA_userdata to base linux system
echo "#############################################################"
echo "# ${cnt}. Release RA , RA_userdata and RA_sysaudios to base linux system"
echo "#############################################################"

#-----------------------------------------------------------
# ${cnt}.1 Release RA to base linux system
echo "# ${cnt}.1 Release RA to base linux system"

# rm old RA file
echo "# rm old RA file"
rm -r -f ./tinalinux/package/nbr/ra/release

# add new release file to tina linux
echo "# add new releasefile to tina linux"
cp -r ra/release ./tinalinux/package/nbr/ra
#cp -r ra/release ./tinalinux/package/nbr/sysctrller

#-----------------------------------------------------------
# ${cnt}.2 Release builtinstory to base linux system
echo "# ${cnt}.2 Release userdata to base linux system"

# rm old userdata file
echo "# rm old userdata file"
rm -r -f ./tinalinux/package/nbr/ra/release_userdata

# get the userdata file to latest version
echo "# get the userdata file to latest version"
cd builtinstory
#./clean.sh
git pull
#./build.sh
cd ${base_path}

# add new release_userdata file to base linux system
echo "# add new release_userdata file to base linux system"
cp -r -f ./builtinstory/release_userdata ./tinalinux/package/nbr/ra

#-----------------------------------------------------------
# ${cnt}.3 Release sysaudio to base linux system
echo "# ${cnt}.3 Release sysaudio to base linux system"

# get the sysaudio file to latest version
echo "# get the sysaudio file to latest version"
cd sysaudio
git pull
./build.sh
cd ${base_path}

# add new release_userdata file to base linux system
echo "# add new release_userdata file to base linux system"
cp -r -f ./sysaudio/release_userdata ./tinalinux/package/nbr/ra

##-----------------------------------------------------------
## ${cnt}.3 Release sysaudios to base linux system
#echo "# ${cnt}.3 Release sysaudios to base linux system"
#
## rm old sysaudios file
#echo "# rm old sysaudios file"
#rm -r -f ./tinalinux/package/nbr/ra/release_sysaudios
#
## get the sysaudios file to latest version
#echo "# get the sysaudios file to latest version"
#cd RA_sysaudios
#git pull
#cd ${base_path}
#
## add new release_sysaudios file to base linux system
#echo "# add new release_sysaudios file to base linux system"
#cp -r -f ./RA_sysaudios/release_sysaudios ./tinalinux/package/nbr/ra
#
#let cnt=cnt+1

# ${cnt}. Build tina linux system
echo "#############################################################"
echo "# ${cnt}. Build tina linux system"
echo "#############################################################"
cd tinalinux
./build_bluetoothphone.sh
cd ${base_path}
let cnt=cnt+1

# ${cnt}. Tag RA application related repos
echo "#############################################################"
echo "# ${cnt}. Tag RA application related repos"
echo "#############################################################"

if [ -n "$1" ] && [ $1 == tag_enable ]; then
    go_repos_list=(`cat go_repos_list`)
    echo ${go_repos_list[@]}
    tina_repos_list=(`cat tina_repos_list`)
    echo ${tina_repos_list[@]}
    tag_version=(`cat ./tinalinux/tagversion.txt`)
    #main_version=${release_version[0]}
    #minor_version=${release_version[1]}
    #tag_version="${main_version}v${minor_version}"

    DATE="`date +%Y-%m-%d`"

    touch update_log.txt
    echo "---------------------------------------------------------------------------" >> update_log.txt
    echo "## ${tag_version}:${DATE}" >> update_log.txt

    for repo in ${go_repos_list[@]}
    do
        cd ${repo}
        git pull
        python ../getcommitNo.py ${repo} ${tag_version} master
        cd ..
    done

    for repo in ${tina_repos_list[@]}
    do
        cd ${repo}
        git pull
        python ../getcommitNo_tina.py ${repo} ${tag_version} r16-v2.1.y
        cd ..
    done
    pwd
    
    mv -f update_log.txt ./rafs_release/${tag_version}/update_log.txt
    if [ -f commitinfo_all.txt ]; then
        mv commitinfo_all.txt ./rafs_release/${tag_version}/commitinfo_all.txt
    fi

    if [ -f commitinfo_all_wo.txt ]; then
        mv -f commitinfo_all_wo.txt ./rafs_release/${tag_version}/commitinfo_all_wo.txt
    fi

    if [ -f commit_single.txt ]; then
        mv -f commit_single.txt ./rafs_release/${tag_version}/commit_single.txt
    fi

    if [ -f gitlog_oneline.txt ]; then
        mv -f gitlog_oneline.txt ./rafs_release/${tag_version}/gitlog_oneline.txt
    fi
else
    echo "Tag all go repos is disable!"
fi

cd ${base_path}
let cnt=cnt+1

# ${cnt}. check ra
echo "#############################################################"
echo "# ${cnt}. check ra"
echo "#############################################################"
echo "ra in ra repo is"`md5sum ./ra/release/usr/bin/ra`
echo "ra in rootfs is" `md5sum ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/usr/bin/ra`
cd ${base_path}
let cnt=cnt+1

# ${cnt}. package ra build result and release 
echo "#############################################################"
echo "# ${cnt}. package ra build result and release"
echo "#############################################################"
boardversion=`cat tinalinux/boardversion.txt`
releaseversion=`cat tinalinux/tagversion.txt`
subreleaseversion="nbrfs_${boardversion}_${releaseversion}"
sudo cp ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/lib/libwifimg.so ./ra/release/lib

# Camera driver related update
mkdir -p ./ra/release//lib/modules/3.4.39
sudo cp ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/lib/modules/3.4.39/vfe_v4l2.ko ./ra/release//lib/modules/3.4.39

# Bluetooth related update
sudo cp ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/usr/lib/libbluetooth.so.3 ./ra/release/usr/lib
sudo cp ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/usr/lib/libdbus-1.so.3 ./ra/release/usr/lib
sudo cp ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/usr/lib/libffi.so.6 ./ra/release/usr/lib
sudo cp ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/usr/lib/libgio-2.0.so.0 ./ra/release/usr/lib
sudo cp ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/usr/lib/libglib-2.0.so.0 ./ra/release/usr/lib
sudo cp ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/usr/lib/libgmodule-2.0.so.0 ./ra/release/usr/lib
sudo cp ./tinalinux/out/astar-nbr/compile_dir/target/rootfs/usr/lib/libgobject-2.0.so.0 ./ra/release/usr/lib

sudo cp ./builtinstory/release_userdata/userdata_built-in.tar.gz ./ra/release/home/root/userdata
sudo cp ./sysaudio/release_userdata/sysaudio_built-in.tar.gz ./ra/release/home/root/userdata
cd ra/release
sudo rm -f usr/bin/otactrller
sudo rm -f usr/bin/audioplay
sudo rm -f *.tar.gz

sudo chmod -R 777 *
sudo chown -R root:root *

sudo tar -zcvf ${subreleaseversion}.tar.gz *
sudo cp ${subreleaseversion}.tar.gz ../../rafs_release/${releaseversion}
cd ${base_path}
let cnt=cnt+1

# ${cnt}. get submit information to sum
if [ -n "$1" ] && [ $1 == tag_enable ]; then
    echo "" >> commit_sum.txt
    cat ./rafs_release/${releaseversion}/update_log.txt >> commit_sum.txt
fi
