#!/bin/bash -e

cnt=1

# ${cnt}. Pull all repos
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "# ${cnt}. Pull all repos"
list=(build config dl docs lichee/brandy  lichee/linux-3.4 package prebuilt scripts target toolchain tools)
basepath=`pwd`
for repo in ${list[@]}
do 
echo $repo
cd $repo
pwd

cd ${basepath}
done
#repo forall -c git checkout *
repo forall -c git pull
repo forall -c git status
repo forall -c git branch -a
let cnt=cnt+1

# ${cnt}. Create tag
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "# ${cnt}. Create tag"
cd scripts
./createtag.sh
cd ..
let cnt=cnt+1

# ${cnt}. Build
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "# ${cnt}. Build"
export FORCE_UNSAFE_CONFIGURE=1
source build/envsetup.sh
lunch astar_nbr-tina
make -j1

echo "---------------------------------------------------------------------------------"
# build 1G card
echo "build 1G card"
cp -f ./target/allwinner/astar-nbr/configs/sys_partition_1.fex ./target/allwinner/astar-nbr/configs/sys_partition.fex
cp -f ./target/allwinner/astar-nbr/configs/sys_partition_nor_1.fex ./target/allwinner/astar-nbr/configs/sys_partition_nor.fex
pack -d


# Release 1G sdcard
echo "# Release 1G sdcard"
pwd

release_version=(`cat tagversion.txt`)
#main_version=${release_version[0]}
#minor_version=${release_version[1]}
mkdir -p ../rafs_release/${release_version}
mv ./out/astar-nbr/nbrfs_P1A_${release_version}.img ../rafs_release/${release_version}/nbrfs_R1A_${release_version}_1G.img


echo "---------------------------------------------------------------------------------"
# build 8G card
echo "build 8G card"
cp -f ./target/allwinner/astar-nbr/configs/sys_partition_8.fex ./target/allwinner/astar-nbr/configs/sys_partition.fex
cp -f ./target/allwinner/astar-nbr/configs/sys_partition_nor_8.fex ./target/allwinner/astar-nbr/configs/sys_partition_nor.fex
pack -d

# Release 8G sdcard
echo "# Release 8G sdcard"
pwd

release_version=(`cat tagversion.txt`)
#main_version=${release_version[0]}
#minor_version=${release_version[1]}
mkdir -p ../rafs_release/${release_version}
mv ./out/astar-nbr/nbrfs_P1A_${release_version}.img ../rafs_release/${release_version}/nbrfs_R1A_${release_version}_8G.img


echo "---------------------------------------------------------------------------------"
# recover to 8G sdcard
echo "recover to 8G sd card"
cp -f ./target/allwinner/astar-nbr/configs/sys_partition_1.fex ./target/allwinner/astar-nbr/configs/sys_partition.fex
cp -f ./target/allwinner/astar-nbr/configs/sys_partition_nor_1.fex ./target/allwinner/astar-nbr/configs/sys_partition_nor.fex

let cnt=cnt+1

