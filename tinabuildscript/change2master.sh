#!/bin/bash -e

cnt=1

# ${cnt}. Pull all repos
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "# ${cnt}. change some tina repo to bluetoothphone"
list_bluetooth=(dl package target)
basepath=`pwd`
for repo in ${list_bluetooth[@]}
do 
echo "------------------------"
echo $repo
cd $repo
pwd
git checkout  r16-v2.1.y
cd ${basepath}
done

#repo forall -c git checkout *
repo forall -c git pull
repo forall -c git status
repo forall -c git branch -a
let cnt=cnt+1
