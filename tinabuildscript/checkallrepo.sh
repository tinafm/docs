#!/bin/bash

list=(build config dl docs lichee/brandy  lichee/linux-3.4 package prebuilt scripts target toolchain tools)
basepath=`pwd`
for repo in ${list[@]}
do 
echo $repo
cd $repo
echo "-----------------------------------"
pwd
git branch -a
git status
cd ${basepath}
done

list1=(dl package target)
basepath=`pwd`
for repo in ${list1[@]}
do 
echo $repo
cd $repo
echo "-----------------------------------"
pwd
git branch -a
git status
cd ${basepath}
done