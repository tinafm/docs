#!/bin/bash -e

cnt=1

# ${cnt}. Pull all repos
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "# ${cnt}. Pull all repos"
list=(build config dl docs lichee/brandy  lichee/linux-3.4 package prebuilt scripts target toolchain tools)
basepath=`pwd`
for repo in ${list[@]}
do 
echo $repo
cd $repo
pwd

cd ${basepath}
done
#repo forall -c git checkout *
repo forall -c git pull
repo forall -c git status
repo forall -c git branch -a
let cnt=cnt+1

# ${cnt}. Create tag
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "# ${cnt}. Create tag"
cd scripts
./createtag.sh
cd ..
let cnt=cnt+1

# ${cnt}. Build
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "# ${cnt}. Build"
source build/envsetup.sh
lunch astar_nbr-tina
make -j1
pack -d

let cnt=cnt+1

# ${cnt}. Release
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo "# ${cnt}. Release"
pwd

release_version=(`cat tagversion.txt`)
#main_version=${release_version[0]}
#minor_version=${release_version[1]}
mkdir -p ../rafs_release/${release_version}
cp ./out/astar-nbr/*${release_version}.img ../rafs_release/${release_version}
